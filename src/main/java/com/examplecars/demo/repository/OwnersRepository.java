package com.examplecars.demo.repository;

import com.examplecars.demo.entity.OwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@NotNull
public interface OwnersRepository extends JpaRepository<OwnerEntity, Integer>,
        JpaSpecificationExecutor<OwnerEntity> {

    OwnerEntity save(OwnerEntity entity);
    List<OwnerEntity> findAll();
    Optional<OwnerEntity> findByFirstName(String firstName);
    Optional<OwnerEntity> findByEmail(String email);
    Optional<OwnerEntity> findByFirstNameAndLastName(String firstName, String lastName);
    void deleteById(Integer Id);
}
