package com.examplecars.demo.repository;

import com.examplecars.demo.entity.CarEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@NotNull
public interface CarsRepository extends JpaRepository<CarEntity, Integer>,
        JpaSpecificationExecutor<CarEntity> {

    CarEntity save(CarEntity entity);
    Optional<CarEntity> findById(Integer id);
    CarEntity findByNumber(String number);
    CarEntity findByBrand(String brand);
    List<CarEntity> findAll();
}