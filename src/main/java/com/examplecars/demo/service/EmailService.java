package com.examplecars.demo.service;

import com.examplecars.demo.entity.CarEntity;
import com.examplecars.demo.entity.OwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    public void sendMessageWhenCarBought(@NotNull OwnerEntity ownerEntity,@NotNull CarEntity carEntity) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(ownerEntity.getEmail());
        message.setSubject("Покупка машины");
        message.setText("Добрый день, "+ ownerEntity.getFirstName() + " " + ownerEntity.getLastName()
                + "\t Вы купили: " + carEntity.getBrand());

        emailSender.send(message);
    }

    public void sendMessageWhenCarSold(@NotNull OwnerEntity ownerEntity,@NotNull CarEntity carEntity) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(ownerEntity.getEmail());
        message.setSubject("Продажа машины");
        message.setText("Добрый день, "+ ownerEntity.getFirstName() + " " + ownerEntity.getLastName()
                + "\t Вы успешно продали: " + carEntity.getBrand());

        emailSender.send(message);
    }
}
