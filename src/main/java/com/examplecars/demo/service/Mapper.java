package com.examplecars.demo.service;

import com.examplecars.demo.entity.CarEntity;
import com.examplecars.demo.entity.OwnerEntity;
import com.examplecars.demo.models.CarModel;
import com.examplecars.demo.models.OwnerModel;

import java.util.ArrayList;

public class Mapper {
    public static CarModel toModel(CarEntity entity){
        CarModel model = new CarModel();
        model.setNumber(entity.getNumber());
        model.setBrand(entity.getBrand());
        model.setDriversQuantity(entity.getDriversQuantity());
        return model;
    }

    public static OwnerModel toModel(OwnerEntity entity){
        OwnerModel model = new OwnerModel();
        model.setFirstName(entity.getFirstName());
        model.setLastName(entity.getLastName());
        model.setEmail(entity.getEmail());
        return model;
    }

    public static CarEntity toEntity(CarModel model){
        CarEntity entity = new CarEntity();
        entity.setBrand(model.getBrand());
        entity.setDriversQuantity(model.getDriversQuantity());
        entity.setNumber(model.getNumber());
        return entity;
    }

    public static OwnerEntity toEntity(OwnerModel model){
        OwnerEntity entity = new OwnerEntity();
        entity.setFirstName(model.getFirstName());
        entity.setLastName(model.getLastName());
        entity.setEmail(model.getEmail());
        entity.setCarEntities(new ArrayList<>());
        return entity;
    }
}
