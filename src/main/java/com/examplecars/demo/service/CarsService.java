package com.examplecars.demo.service;

import com.examplecars.demo.entity.CarEntity;
import com.examplecars.demo.models.CarModel;
import com.examplecars.demo.repository.CarsRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class CarsService {
    private final CarsRepository carsRepository;

    public CarsService(CarsRepository carsRepository) {
        this.carsRepository = carsRepository;
    }

    public void addCar(CarModel model) {
        carsRepository.save(Mapper.toEntity(model));
    }

    public List<CarEntity> getCarsEntities(){
        return carsRepository.findAll();
    }

    public void deleteCar(Integer id){
        carsRepository.deleteById(id);
    }

    public void editCar(CarModel model) {
        var entity = carsRepository.findByBrand(Mapper.toEntity(model).getBrand());

        if(!entity.getBrand().isEmpty()){
            entity.setBrand(model.getBrand());
        }
        if(!entity.getNumber().isEmpty()){
            entity.setNumber(model.getNumber());
        }
        if(!entity.getDriversQuantity().equals(0)){
            entity.setDriversQuantity(model.getDriversQuantity());
        }
        carsRepository.save(entity);
    }

    public CarModel getCarByNumber(String number){
        return Mapper.toModel(carsRepository.findByNumber(number));
    }

    public CarModel getCarByBrand(String brand){
        return Mapper.toModel(carsRepository.findByBrand(brand));
    }

    public List<CarModel> getCars(){
        var carEntities = carsRepository.findAll();
        List<CarModel> carModels = new ArrayList<>();

        for (CarEntity item: carEntities) {
            carModels.add(Mapper.toModel(item));
        }
        return carModels;
    }
}
