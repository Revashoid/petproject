package com.examplecars.demo.service;

import com.examplecars.demo.entity.CarEntity;
import com.examplecars.demo.entity.OwnerEntity;
import com.examplecars.demo.models.CarModel;
import com.examplecars.demo.models.OwnerModel;
import com.examplecars.demo.repository.CarsRepository;
import com.examplecars.demo.repository.OwnersRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class OwnersService {
    private final OwnersRepository ownersRepository;
    private final CarsRepository carsRepository;
    private final EmailService emailService;

    public OwnersService(OwnersRepository ownersRepository, CarsRepository carsRepository, EmailService emailService) {
        this.ownersRepository = ownersRepository;
        this.carsRepository = carsRepository;
        this.emailService = emailService;
    }

    public List<OwnerEntity> getOwnersEntities(){
        return ownersRepository.findAll();
    }

    public List<OwnerModel> getOwners(){
        var ownerEntities = ownersRepository.findAll();
        List<OwnerModel> ownerModels = new ArrayList<>();

        for (OwnerEntity item: ownerEntities) {
            ownerModels.add(Mapper.toModel(item));
        }
        return ownerModels;
    }

    public OwnerModel getOwnerByFirstName(String firstName){
        return Mapper.toModel(ownersRepository.findByFirstName(firstName)
                .orElseThrow(() -> new ResourceNotFoundException("Owner with name " + firstName + " not found.")));
    }

    public void addOwner(OwnerModel model){
        ownersRepository.save(Mapper.toEntity(model));
    }

    public void addCarToOwnerById(Integer carId, Integer ownerId) {
        var optionalOwnerEntity = ownersRepository.findById(ownerId);
        var ownerEntity = optionalOwnerEntity
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found."));

        var optionalCarEntity = carsRepository.findById(carId);
        var carEntity = optionalCarEntity
                .orElseThrow(() -> new ResourceNotFoundException("Car not found."));

        var carEntities = ownerEntity.getCarEntities();
        carEntities.add(carEntity);
        ownerEntity.setCarEntities(carEntities);

        emailService.sendMessageWhenCarBought(ownerEntity, carEntity);
        ownersRepository.save(ownerEntity);
    }

    public void editOwnerEmail(String lastEmail, String newEmail) {
        var ownerEntity = ownersRepository.findByEmail(lastEmail).stream().findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found."));

        Objects.requireNonNull(ownerEntity).setEmail(newEmail);
        ownersRepository.save(ownerEntity);
    }

    public void editOwnerFirstNameAndLastName(OwnerModel model, String newFirstName, String newLastName) {
        var ownerEntity = ownersRepository
                .findByFirstNameAndLastName(model.getFirstName(), model.getLastName())
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found."));

        Objects.requireNonNull(ownerEntity).setFirstName(newFirstName);
        Objects.requireNonNull(ownerEntity).setLastName(newLastName);
        ownersRepository.save(ownerEntity);
    }

    public void deleteOwnerById(Integer ownerId) {
        ownersRepository.deleteById(ownerId);
    }

    public List<CarModel> getOwnerCars(String firstName, String lastName) {
        var ownerEntity = ownersRepository.findByFirstNameAndLastName(firstName, lastName)
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found."));

        var carsEntities = ownerEntity.getCarEntities();
        List<CarModel> carModels = new ArrayList<>();

        for (CarEntity item: carsEntities) {
            carModels.add(Mapper.toModel(item));
        }

        return carModels;
    }

    public void transferCarToAnotherOwnerByBrand(String brand, String oldOwnerFirstName
            , String oldOwnerLastName,String newOwnerFirstName, String newOwnerLastName) {
        var carEntity = carsRepository.findByBrand(brand);
        var newOwnerEntity = ownersRepository.findByFirstNameAndLastName(newOwnerFirstName, newOwnerLastName)
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found."));
        var oldOwnerEntity = ownersRepository.findByFirstNameAndLastName(oldOwnerFirstName, oldOwnerLastName)
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found."));

        deleteCarFromOwner(oldOwnerEntity, carEntity);
        emailService.sendMessageWhenCarSold(oldOwnerEntity, carEntity);
        newOwnerEntity.getCarEntities().add(carEntity);
        emailService.sendMessageWhenCarBought(newOwnerEntity, carEntity);
        ownersRepository.save(newOwnerEntity);
    }

    public void deleteCarFromOwnerByBrand(String brand, String firstName, String lastName) {
        var ownerEntity = ownersRepository.findByFirstNameAndLastName(firstName, lastName)
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found."));
        var carEntity = carsRepository.findByBrand(brand);

        Objects.requireNonNull(ownerEntity).getCarEntities().remove(carEntity);
        emailService.sendMessageWhenCarSold(ownerEntity, carEntity);
        ownersRepository.save(ownerEntity);
    }

    private void deleteCarFromOwner(OwnerEntity ownerEntity, CarEntity carEntity){
        ownerEntity.getCarEntities().remove(carEntity);
        ownersRepository.save(ownerEntity);
    }
}
