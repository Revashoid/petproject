package com.examplecars.demo.service;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
@Aspect
public class LoggingAspect {
    private final Logger logger = Logger.getLogger(LoggingAspect.class.getName());

    @Pointcut("within(com.examplecars.demo.service.CarsService)")
    public void carsProcessingMethods() {
    }

    @Pointcut("within(com.examplecars.demo.service.OwnersService)")
    public void ownersProcessingMethods() {
    }

    @After("carsProcessingMethods() || ownersProcessingMethods()")
    public void logMethodCall(JoinPoint jp) {
        String methodName = jp.getSignature()
                .getName();
        logger.log(Level.INFO, "Вызван метод: " + methodName + "()");
        logger.log(Level.INFO, "Тип метода: " + jp.getSignature());
    }

    @AfterReturning(pointcut = "execution(public * com.examplecars.demo.service.CarsService.*(..)) " +
            "|| (com.examplecars.demo.service.LoggingAspect.ownersProcessingMethods())", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {
        if(result != null) {
            logger.log(Level.INFO, "Возвращено значение: " + result.toString());
        }
        else{
            logger.log(Level.INFO, "Отсутствует возвращаемое значение");
        }
    }

    @AfterReturning(pointcut = "execution(public * com.examplecars.demo.service.CarsService.editCar())", returning = "car")
    public void logAfterReturningFromCar(JoinPoint joinPoint, Object car){
        if(car != null) {
            logger.log(Level.INFO, "Изменено состояние машины на: " + car.toString());
        }
        else{
            logger.log(Level.INFO, "С машиной не произошло изменений");
        }
    }

    @Pointcut("execution(* com.examplecars.demo.service.OwnersService.addCarToOwnerById(..)) && args(list,..))")
    public void callAtOwnerServiceWhenCarAddedToOwner(List<Integer> list) { }

    @AfterReturning(value = "callAtOwnerServiceWhenCarAddedToOwner(list)", argNames = "list")
    public void afterCallAtOwnerServiceWhenCarAddedToOwner(List<Integer> list) {
        if(list != null) {

        }
    }
}
