package com.examplecars.demo.models;

public class CarModel {
    private String number;
    private String brand;
    private Integer driversQuantity;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getDriversQuantity() {
        return driversQuantity;
    }

    public void setDriversQuantity(Integer driversQuantity) {
        this.driversQuantity = driversQuantity;
    }

    @Override
    public String toString() {
        return "CarModel{" +
                "number='" + number + '\'' +
                ", brand='" + brand + '\'' +
                ", driversQuantity=" + driversQuantity +
                '}';
    }
}
