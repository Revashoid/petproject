package com.examplecars.demo.controller;

import com.examplecars.demo.entity.OwnerEntity;
import com.examplecars.demo.models.CarModel;
import com.examplecars.demo.models.OwnerModel;
import com.examplecars.demo.service.OwnersService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OwnersController {
    private final OwnersService ownersService;

    public OwnersController(OwnersService ownersService) {
        this.ownersService = ownersService;
    }

    @GetMapping("/getOwnersEntities")
    public List<OwnerEntity> getOwnersEntities(){
        return ownersService.getOwnersEntities();
    }

    @GetMapping("/getOwners")
    public List<OwnerModel> getOwners(){
        return ownersService.getOwners();
    }

    @GetMapping("/getOwnerCars")
    public List<CarModel> getOwnerCars(@RequestParam String firstName, @RequestParam String lastName){
        return ownersService.getOwnerCars(firstName, lastName);
    }

    @GetMapping("/getOwners/{firstName}")
    public OwnerModel getOwnerByFirstName(@PathVariable(value = "firstName") String firstName){
        return ownersService.getOwnerByFirstName(firstName);
    }

    @PostMapping("/addOwner")
    public void addOwner(@RequestBody OwnerModel model){
        ownersService.addOwner(model);
    }

    @PutMapping("/addCar")
    public void addCarToOwnerById(Integer carId, Integer ownerId){
        ownersService.addCarToOwnerById(carId, ownerId);
    }


    @PutMapping("/transferCar")
    public void transferCarToNewOwnerByBrand(@RequestParam String brand
            ,@RequestParam String oldOwnerFirstName
            ,@RequestParam String oldOwnerLastName
            ,@RequestParam String newOwnerFirstName
            ,@RequestParam String newOwnerLastName){
        ownersService.transferCarToAnotherOwnerByBrand(brand, oldOwnerFirstName, oldOwnerLastName
                , newOwnerFirstName, newOwnerLastName);
    }

    @PatchMapping("/editOwnerEmail")
    public void editOwnerEmail(@RequestParam String lastEmail
                              ,@RequestParam String newEmail){
        ownersService.editOwnerEmail(lastEmail, newEmail);
    }

    @PatchMapping("/editOwner/{firstName}/{lastName}")
    public void editOwnerFirstNameAndLastName(@RequestBody  OwnerModel model
                          ,@PathVariable(value = "firstName") String newFirstName
                          ,@PathVariable(value = "lastName") String newLastName){
        ownersService.editOwnerFirstNameAndLastName(model, newFirstName, newLastName);
    }

    @DeleteMapping("/deleteOwner")
    public void deleteOwnerById(@RequestParam Integer ownerId){
        ownersService.deleteOwnerById(ownerId);
    }

    @DeleteMapping("/deleteCar{brand}")
    public void deleteCarFromOwnerByBrand(@PathVariable(value = "brand") String brand
                                        ,@RequestParam String firstName
                                        ,@RequestParam String lastName){
        ownersService.deleteCarFromOwnerByBrand(brand, firstName, lastName);
    }
}
