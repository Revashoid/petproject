package com.examplecars.demo.controller;

import com.examplecars.demo.entity.CarEntity;
import com.examplecars.demo.models.CarModel;
import com.examplecars.demo.service.CarsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CarsController {
    private final CarsService carsService;

    public CarsController(CarsService carsService) {
        this.carsService = carsService;
    }

    @GetMapping("/getCars")
    public List<CarModel> getCars(){
        return carsService.getCars();
    }

    @GetMapping("/getCarsEntities")
    public List<CarEntity> getCarsEntities(){
        return carsService.getCarsEntities();
    }

    @GetMapping("/getCars/{number}")
    public CarModel getCarByNumber(@PathVariable(value = "number") String number){
        return carsService.getCarByNumber(number);
    }

    @GetMapping("/getCar/{brand}")
    public CarModel getCarByBrand(@PathVariable(value = "brand") String brand) {
        return carsService.getCarByBrand(brand);
    }

    @PostMapping("/addCar")
    public void addCar(CarModel model){
        carsService.addCar(model);
    }

    @DeleteMapping("/deleteCar/{id}")
    public void deleteCarById(@PathVariable(value = "id")Integer id){
        carsService.deleteCar(id);
    }

    @PatchMapping("/editCar")
    public void editCar(@RequestBody CarModel model){
        carsService.editCar(model);
    }
}
