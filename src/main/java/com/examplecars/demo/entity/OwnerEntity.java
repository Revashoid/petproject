package com.examplecars.demo.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Owners")
public class OwnerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "propSeq")
    @SequenceGenerator(name = "propSeq", sequenceName = "property_sequence", allocationSize = 1)
    private Integer Id;

    private String firstName;
    private String lastName;
    private String email;

    @OneToMany
    private List<CarEntity> carEntities;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<CarEntity> getCarEntities() {
        return carEntities;
    }

    public void setCarEntities(List<CarEntity> carEntities) {
        this.carEntities = carEntities;
    }

    @Override
    public String toString() {
        return "OwnerEntity{" +
                "Id=" + Id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", carEntities=" + carEntities +
                '}';
    }
}
