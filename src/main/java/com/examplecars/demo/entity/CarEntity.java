package com.examplecars.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "Cars")
public class CarEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "propSeq")
    @SequenceGenerator(name = "propSeq", sequenceName = "property_sequence", allocationSize = 1)
    private Integer Id;
    private String number;
    private String brand;
    private Integer driversQuantity;

    public String getNumber() {
        return number;
    }

    public void setNumber(String carNumber) {
        this.number = carNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String carModel) {
        this.brand = carModel;
    }

    public Integer getDriversQuantity() {
        return driversQuantity;
    }

    public void setDriversQuantity(Integer driversQuantity) {
        this.driversQuantity = driversQuantity;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    @Override
    public String toString() {
        return "CarEntity{" +
                "Id=" + Id +
                ", number='" + number + '\'' +
                ", brand='" + brand + '\'' +
                ", driversQuantity=" + driversQuantity +
                '}';
    }
}
